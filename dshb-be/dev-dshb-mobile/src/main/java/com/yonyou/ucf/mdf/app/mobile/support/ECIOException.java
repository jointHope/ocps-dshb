package com.yonyou.ucf.mdf.app.mobile.support;

/**
 * @author liuhaoi
 */
public class ECIOException extends ECException {

    public ECIOException(String message) {
        super(message);
    }

    public ECIOException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public int getCode() {
        return ECErrorCode.IO_ERROR;
    }
}
