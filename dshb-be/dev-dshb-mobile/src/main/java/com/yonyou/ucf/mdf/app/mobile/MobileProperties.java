package com.yonyou.ucf.mdf.app.mobile;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class MobileProperties {

    /**
     * 解密空间yhtToken的私钥
     */
    @Value("${mdf.mobile.yhtTokenPrivateKey}")
    private String yhtTokenPrivateKey;

    /**
     * 移动端允许跨域的域名后缀
     */
    private String[] crossDomainWhitelist = {"yyuap.com", "diwork.com", "yonyoucloud.com", "yonyou.com"};

    /**
     * 请求移动端菜单列表的域名
     */
    @Value("${domain.iuap-yonbuilder-designer}")
    private String appMenuListHost;


}
