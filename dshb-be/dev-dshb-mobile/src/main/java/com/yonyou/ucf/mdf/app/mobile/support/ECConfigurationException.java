package com.yonyou.ucf.mdf.app.mobile.support;

/**
 * @author liuhaoi
 */
public class ECConfigurationException extends ECException {

    public ECConfigurationException(String message) {
        super(message);
    }

    public ECConfigurationException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public int getCode() {
        return ECErrorCode.SERVER_CONFIGURATION_ERROR;
    }

}
