package com.yonyou.ucf.mdf.app.mobile.support;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * @author liuhaoi
 */
public class ECAuthenticationFailedException extends ECException {

    public ECAuthenticationFailedException(String message) {
        super(message);
    }

    public ECAuthenticationFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public int getCode() {
        return ECErrorCode.AUTHENTICATION_FAILED;
    }

    @Override
    public Map<Locale, String> getDisplayMessage() {
        Map<Locale, String> message = new HashMap<>(3);

        message.put(Locale.SIMPLIFIED_CHINESE, "验证未通过,请求拒绝");
        message.put(Locale.TRADITIONAL_CHINESE, "驗證未通過，請求拒絕");
        message.put(Locale.US, "Validation failed, request rejected.");

        return message;
    }

}
