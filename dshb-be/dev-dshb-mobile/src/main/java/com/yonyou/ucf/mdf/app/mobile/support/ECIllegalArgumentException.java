package com.yonyou.ucf.mdf.app.mobile.support;

/**
 * @author liuhaoi
 */
public class ECIllegalArgumentException extends ECException {

    public ECIllegalArgumentException(String message) {
        super(message);
    }

    public ECIllegalArgumentException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public int getCode() {
        return ECErrorCode.ILLEGAL_ARGUMENT;
    }



}
