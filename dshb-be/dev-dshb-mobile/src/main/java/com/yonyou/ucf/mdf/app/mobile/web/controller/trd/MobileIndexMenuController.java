package com.yonyou.ucf.mdf.app.mobile.web.controller.trd;

import com.alibaba.fastjson.JSON;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.ucf.mdd.ext.controller.Authentication;
import com.yonyou.ucf.mdf.app.mobile.MobileProperties;
import com.yonyou.ucf.mdf.app.mobile.module.MobileCrossOriginStrategy;
import com.yonyou.ucf.mdf.app.mobile.web.controller.pojo.CommonResponse;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequest;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequestBody;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Authentication(false)
@Slf4j
@Controller
@RequiredArgsConstructor
@RequestMapping("/mobile/app/")
public class MobileIndexMenuController {

    private static final String PATH_APP_MENU = "hpamenu/getMenuListByMobile?appCode=%s";

    private final MobileProperties properties;

    private final MobileCrossOriginStrategy mobileCrossOriginStrategy;

    private final ISVRequest isvRequest;


    @GetMapping(value = "/menu/list", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public void getMenuList(@RequestParam String appCode, @CookieValue("wb_at") String webAccessToken, HttpServletRequest request,
                            HttpServletResponse response) throws IOException {
        String appMenuListHost = properties.getAppMenuListHost();
        String split;
        if (appMenuListHost.endsWith("/")) {
            split = "";
        } else {
            split = "/";
        }
        String url = appMenuListHost + split + String.format(PATH_APP_MENU, appCode);

        mobileCrossOriginStrategy.writeAllowCrossDomainHeaders(request, response);
        response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
        ISVRequestBody isvRequestBody = new ISVRequestBody();
        isvRequestBody.setUrl(url);
        isvRequestBody.setRequestMethod("GET");
        Map<String, String> headers = new HashMap<>();
        StringBuilder cookieStr = new StringBuilder();
        cookieStr.append("wb_at=").append("=").append(webAccessToken).append(";");
        cookieStr.append("Content-Type").append("=").append("application/json").append(";");
        headers.put("Cookie", cookieStr.toString());
        isvRequestBody.setHeaders(headers);
        String result = "";
        try {
            result = isvRequest.doRequest(InvocationInfoProxy.getTenantid(), isvRequestBody);
        } catch (Exception e) {
            response.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
            response.getWriter().write(JSON.toJSONString(CommonResponse.failed("request app menu failed", "500")));
            return;
        }
        response.setHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE);
        response.getWriter().write(Objects.requireNonNull(result));
    }

}
