package com.yonyou.ucf.mdf.app.mobile;


import com.yonyou.diwork.config.DiworkEnv;
import com.yonyou.workbench.util.Lists;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.CharacterEncodingFilter;


@Configuration
public class MobileConfiguration {
    @Bean
    public FilterRegistrationBean<CharacterEncodingFilter> filterRegistrationBean() {
        FilterRegistrationBean<CharacterEncodingFilter> registrationBean = new FilterRegistrationBean<>();
        CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
        characterEncodingFilter.setForceEncoding(true);
        characterEncodingFilter.setEncoding("UTF-8");
        registrationBean.setFilter(characterEncodingFilter);
        registrationBean.setUrlPatterns(Lists.newArrayList("/mobile/*"));
        return registrationBean;
    }

    @Bean
    @ConditionalOnMissingBean
    public DiworkEnv diworkEnv(){
        return new DiworkEnv();
    }
}
