/*
package com.yonyou.ucf.mdf.app.controller;

import com.google.gson.JsonObject;
import com.yonyou.ucf.mdd.ext.base.BaseController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

*/
/**
 * @author liangrch@yonyou.com
 * @version 1.0
 * @createTime 2021/7/20 11:10
 * @description
 *//*

@RestController
@RequestMapping(value = "/extend")
public class ExtendController extends BaseController {

    private Logger logger = LoggerFactory.getLogger(ExtendController.class);

    private static final String YHTACCESSTOKEN = "yht_access_token";


    // 健康检查
    @RequestMapping(value = "/healthycheck", method = {RequestMethod.POST, RequestMethod.GET})
    public void testConnect(HttpServletRequest request, HttpServletResponse response) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("status", "ok");
        jsonObject.addProperty("msg", "Connected!!!");
        renderJson(response, jsonObject.toString());
    }

}
*/
