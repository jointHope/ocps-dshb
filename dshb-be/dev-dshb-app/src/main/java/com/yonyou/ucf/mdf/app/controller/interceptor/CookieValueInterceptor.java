/*
package com.yonyou.ucf.mdf.app.controller.interceptor;

import com.yonyou.ucf.mdf.app.service.impl.ISVAuthService;
import com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil;
import com.yonyoucloud.iuap.ucf.mdd.starter.core.module.data.cache.StringKeyCache;
import com.yonyoucloud.iuap.ucf.mdd.starter.core.module.data.cache.redis.UCFRedisCacheFactory;
import com.yonyoucloud.iuap.ucf.mdd.starter.token.service.TokenService;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.auth.CommonAuthApi;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.auth.pojo.TokenResult;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.PostConstruct;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.Duration;
import java.util.Arrays;

import static com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil.YHT_ACCESS_TOKEN;
import static com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil.clearContext;

@Slf4j
@Component
@RequiredArgsConstructor
public class CookieValueInterceptor extends HandlerInterceptorAdapter {


    private final ISVBuildContextUtil isvBuildContextUtil;


    protected final UCFRedisCacheFactory cacheFactory;


    @Override
    public boolean preHandle(HttpServletRequest request, @NotNull HttpServletResponse response,
                             @NotNull Object handler) {
        Cookie[] cookies = request.getCookies();
        String serviceCode = request.getParameter("serviceCode");
        if (cookies != null) {
            buildContextByCookies(response, cookies, serviceCode);
        }
        return true;
    }

    private void buildContextByCookies(HttpServletResponse response, Cookie[] cookies, String serviceCode) {
//直接把serviceCode写进上下文
        buildContextByYhtCookie(cookies, serviceCode);
    }

    // （使用统一node后）兼容获取yyuap域名下的yht_access_token的cookie信息
    private void buildContextByYhtCookie(Cookie[] cookies, String serviceCode) {
        Cookie yhtTokenCookie = Arrays.stream(cookies)
                .filter(cookie -> YHT_ACCESS_TOKEN.equals(cookie.getName()))
                .findAny()
                .orElse(null);
        if (yhtTokenCookie == null) {
            log.error("CookieValueInterceptor友互通token不存在");
//            throw new RuntimeException("CookieValueInterceptor友互通token不存在，请重新登录");
        } else {
            try {
                isvBuildContextUtil.buildContextByYhtAccessToken(yhtTokenCookie.getValue(), serviceCode);
            } catch (Exception e) {
                log.error("使用yhtAccessToken设置用户上下文信息失败，{},{}", e.getMessage(), e);
            }
        }
    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request,
                                @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {
        clearContext();
    }
}
*/
