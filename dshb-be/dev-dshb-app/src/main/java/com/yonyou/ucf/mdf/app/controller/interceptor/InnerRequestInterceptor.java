/*
package com.yonyou.ucf.mdf.app.controller.interceptor;

import com.yonyoucloud.iuap.ucf.mdd.starter.authentication.UCFAuthenticationProperties;
import com.yonyoucloud.iuap.ucf.mdd.starter.core.module.network.ClientAddressUtil;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import java.util.List;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;
import static com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil.*;


*/
/**
 * 用于拦截内部服务（比如编码规则）请求isv数据的接口，设置上下文，配置ip白名单（此拦截器暂未配置）
 *//*

@Component
@Slf4j
@RequiredArgsConstructor
public class InnerRequestInterceptor extends HandlerInterceptorAdapter {

  private final UCFAuthenticationProperties properties;

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
          throws Exception {
    String ipAddress = ClientAddressUtil.getLatestProxyIpAddress(request);
    List<Pattern> ipWhiteListRegexPattern = properties.getIpWhiteListRegexPattern();
    for (Pattern pattern : ipWhiteListRegexPattern){
      Matcher matcher = pattern.matcher(ipAddress);
      if (matcher.find()){
        return true;
      }
    }
    return false;
  }

  @Override
  public void afterCompletion(@NotNull HttpServletRequest request,
                              @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {
    clearContext();
  }

}
*/
