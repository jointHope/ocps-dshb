/*
package com.yonyou.ucf.mdf.app.controller.interceptor;

import com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.auth.CommonAuthApi;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.yonyou.ucf.mdf.app.util.ISVBuildContextUtil.*;


*/
/**
 * @Author: LuoYu
 * @Date: 2021/6/8 14:57
 * @Description: 用于拦截审批流和业务流等回调接口的header的yhtAccessToken认证信息，设置用户上下文信息
 *//*

@Slf4j
@Component
@RequiredArgsConstructor
public class HeaderInterceptor extends HandlerInterceptorAdapter {

    private final CommonAuthApi authApi;

    private final ISVBuildContextUtil isvBuildContextUtil;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response,
        Object handler) throws Exception {
        String yhtAccessToken = request.getHeader(YHT_ACCESS_TOKEN);
        if (StringUtils.isNotBlank(yhtAccessToken)){
            return isvBuildContextUtil.buildContextByYhtAccessToken(yhtAccessToken, "");
        }
        return true;
    }

    @Override
    public void afterCompletion(@NotNull HttpServletRequest request,
        @NotNull HttpServletResponse response, @NotNull Object handler, Exception ex) {
        clearContext();
    }
}
*/
