package com.yonyou.ucf.mdf.domain.rule;

import com.yonyou.ucf.mdd.common.model.rule.RuleExecuteResult;
import com.yonyou.ucf.mdd.ext.bill.dto.BillDataDto;
import com.yonyou.ucf.mdd.ext.bill.rule.base.AbstractCommonRule;
import com.yonyou.ucf.mdd.ext.bill.rule.util.BillInfoUtils;
import com.yonyou.ucf.mdd.ext.model.BillContext;
import com.yonyou.ucf.mdd.ext.util.DomainUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author liangrch@yonyou.com
 * @version 1.0
 * @createTime 2021/7/20 9:50
 * @description 本类主要用于交易类型参照过滤条件
 */
@Component("mddBeforeRefDataRule")
public class MddBeforeRefDataRule extends AbstractCommonRule {

    private static final String REFCODE_BD_BILLTYPEREF = "bd_billtyperef";
    private static final String FIELD_BILLTYPE_FORMID = "billtype_id.form_id";

    @Override
    public RuleExecuteResult execute(BillContext billContext, Map<String, Object> map) throws Exception {
        if (null == billContext) {
            return new RuleExecuteResult();
        }
        BillDataDto billData = (BillDataDto) getParam(map);
        String refCode = billData.getRefCode();
        if (StringUtils.isNotBlank(refCode) && refCode.indexOf(".")> -1) {
            String[] arr = refCode.split("\\.");
            refCode = arr[1] ;
        }
        String billNum = billContext.getBillnum();
        String billTypeFormId;
        if (StringUtils.isNotBlank(billContext.getDomain())) {
            billTypeFormId = billContext.getDomain() + "." + billNum;
        }else {
            billTypeFormId = DomainUtils.getAppName() + "." + billNum;
        }
        if (REFCODE_BD_BILLTYPEREF.equals(refCode)) {
            BillInfoUtils.appendCondition(billData, FIELD_BILLTYPE_FORMID, "eq", billTypeFormId);
        }
        return new RuleExecuteResult();
    }
}
