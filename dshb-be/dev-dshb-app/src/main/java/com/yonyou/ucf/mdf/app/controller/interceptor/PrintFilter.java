package com.yonyou.ucf.mdf.app.controller.interceptor;

import com.yonyou.diwork.service.pub.ITenantUserService;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.ucf.mdd.ext.util.Logger;
import com.yonyou.workbench.model.TenantVO;
import com.yonyou.workbench.model.UserVO;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.*;
import java.io.IOException;

@Slf4j
public class PrintFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        try {
            WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(request.getServletContext());
            ITenantUserService iTenantUserService = applicationContext.getBean(ITenantUserService.class);

            UserVO userVO = iTenantUserService.getUserByUserId(InvocationInfoProxy.getUserid());
            InvocationInfoProxy.setExtendAttribute("userName", userVO.getUserName());
            TenantVO tenantVO = iTenantUserService.getTenant(InvocationInfoProxy.getTenantid());
            InvocationInfoProxy.setExtendAttribute("tenantName", tenantVO.getTenantName());
        } catch (Exception e) {
            Logger.error("PrintFilter异常: ", e);
        } finally {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }

}
