/*
package com.yonyou.ucf.mdf.app.service.impl;

import com.yonyou.ucf.mdd.common.constant.MddConstants;
import com.yonyou.ucf.mdd.common.model.uimeta.ui.Entity;
import com.yonyou.ucf.mdd.common.model.uimeta.ui.Field;
import com.yonyou.ucf.mdd.common.model.uimeta.ui.ViewModel;
import com.yonyou.ucf.mdd.ext.poi.model.ExcelExportData;
import com.yonyou.ucf.mdd.ext.poi.service.IPOIExtService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Map;

*/
/**
 * @author liangrch@yonyou.com
 * @version 1.0
 * @createTime 2021/9/3 10:19
 * @description
 *//*

@Service
public class IPOIExtServiceImpl implements IPOIExtService {

    private final  String CMODELTYPE = "TagModel";

    @Override
    public void beforeFetchTemplate(Map<String, Object> params) {

    }

    @Override
    public void afterFetchTemplate(ViewModel viewModel) {

    }

    @Override
    public void afterFetchTemplate(ViewModel viewModel, Map<String, Object> params) {
        LinkedHashSet<Entity> entities = viewModel.getEntities();
        if (CollectionUtils.isNotEmpty(entities)) {
            Iterator<Entity> iterator = entities.iterator();
            while(iterator.hasNext()) {
                Entity entity = iterator.next();
                if ( MddConstants.TYPE_ENTITY_BILL.equalsIgnoreCase(entity.getType()) && CMODELTYPE.equalsIgnoreCase(entity.getModelType())) {
                    Field field = new Field();
                    BeanUtils.copyProperties(entity.getFields().get(0),field);
                    field.setFieldName(entity.getForeignKey());
                    field.setItemName(entity.getForeignKey());
                    field.setCaption("多选外键手工码");
                    field.setShowCaption("多选外键手工码");
                    entity.getFields().add(field);
                }
            }

        }

    }

    @Override
    public void afterCreateExcel(Workbook workbook, ExcelExportData excelExportData) {

    }
}
*/
