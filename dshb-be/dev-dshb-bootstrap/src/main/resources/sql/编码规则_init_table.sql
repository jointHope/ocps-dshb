
--
-- Drop table aa_billcode_candidateprop
--
DROP TABLE IF EXISTS aa_billcode_candidateprop;

--
-- Drop table aa_billcode_mapping
--
DROP TABLE IF EXISTS aa_billcode_mapping;

--
-- Drop table aa_billcode_mappingchild
--
DROP TABLE IF EXISTS aa_billcode_mappingchild;

--
-- Drop table aa_billcode_obj
--
DROP TABLE IF EXISTS aa_billcode_obj;

--
-- Drop table aa_billhistory
--
DROP TABLE IF EXISTS aa_billhistory;

--
-- Drop table aa_billnumber
--
DROP TABLE IF EXISTS aa_billnumber;

--
-- Drop table aa_billprecode
--
DROP TABLE IF EXISTS aa_billprecode;

--
-- Drop table aa_billprefabricate
--
DROP TABLE IF EXISTS aa_billprefabricate;

--
-- Drop table aa_billprefix
--
DROP TABLE IF EXISTS aa_billprefix;

--
-- Drop table aa_billreturn
--
DROP TABLE IF EXISTS aa_billreturn;


--
-- Create table aa_billreturn
--
CREATE TABLE aa_billreturn (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  tenant_id BIGINT(20) DEFAULT 0 COMMENT '租户',
  orgId VARCHAR(64) NOT NULL DEFAULT -1 COMMENT '组织',
  cbillnum VARCHAR(50) DEFAULT NULL COMMENT '表单编码',
  ownerorg BIGINT(20) DEFAULT NULL COMMENT '所属组织',
  cglide VARCHAR(250) DEFAULT NULL COMMENT '前缀名称',
  cgliderule VARCHAR(250) DEFAULT NULL COMMENT '取值规则',
  cseed VARCHAR(250) NOT NULL COMMENT '编号前缀',
  inumber INT(11) NOT NULL COMMENT '流水号',
  totalBasis VARCHAR(500) NOT NULL COMMENT '总依据(依据1|依据2|依据3|...)',
  billnumberid INT(11) NOT NULL COMMENT '规则id（外键）',
  dr TINYINT(1) DEFAULT NULL COMMENT '删除标记',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间戳',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户',
  billnumberCode VARCHAR(50) NOT NULL COMMENT '规则code',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '退号表',
ROW_FORMAT = COMPACT;

--
-- Create table aa_billprefix
--
CREATE TABLE aa_billprefix (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  tenant_id BIGINT(20) DEFAULT 0 COMMENT '租户',
  orgId VARCHAR(64) NOT NULL DEFAULT -1 COMMENT '组织',
  cprefix VARCHAR(100) NOT NULL COMMENT '编码段名称：如门店代码，手工输入等',
  iprefixlen INT(11) NOT NULL COMMENT '长度',
  cprefixrule VARCHAR(50) DEFAULT NULL COMMENT '取值规则(例如yyyymmdd)',
  cprefixseed VARCHAR(50) DEFAULT NULL COMMENT '依据参数：如y，ym，ymd',
  iorder INT(11) NOT NULL DEFAULT 0 COMMENT '排序',
  bfix TINYINT(1) NOT NULL DEFAULT 0 COMMENT '是否依据',
  cprefixid VARCHAR(50) DEFAULT NULL COMMENT '关联前缀预设的id',
  cprefixtype INT(11) NOT NULL DEFAULT 1 COMMENT '前缀类型 0(字符字段)、1（日期字段）、2（常量）、3(系统时间)，4（单据参照属性），5 随机码',
  cprefixsep VARCHAR(10) DEFAULT NULL COMMENT '分隔符',
  cfieldname VARCHAR(50) DEFAULT NULL COMMENT '取值字段',
  csourcename VARCHAR(50) DEFAULT NULL COMMENT 'aa_billprefabricate表里的csourcename',
  ipurpose TINYINT(4) DEFAULT NULL COMMENT '和预置表aa_billprefabricate里的ipurpose字段保持一致',
  fillstyle TINYINT(4) DEFAULT NULL COMMENT '补位方式（0-不补位，1-左补位，2-右补位）',
  fillsign VARCHAR(4) DEFAULT NULL COMMENT '补位符（最长4位）  ',
  billnumberid INT(11) DEFAULT NULL COMMENT '规则id',
  dr TINYINT(1) DEFAULT NULL COMMENT '删除标记',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间戳',
  cbillnum VARCHAR(50) DEFAULT NULL COMMENT 'aa_billnumber表的cbillnum字段一致',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户',
  formula VARCHAR(500) DEFAULT NULL COMMENT '公式',
  formuladisplay VARCHAR(500) DEFAULT NULL COMMENT '公式显示',
  bMain TINYINT(4) DEFAULT '1' COMMENT '是否主表字段，0否 1是',
  billnumberCode VARCHAR(50) NOT NULL COMMENT '规则code',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 24663,
AVG_ROW_LENGTH = 334,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '编码规则子表',
ROW_FORMAT = COMPACT;

--
-- Create index ix_aa_billprefix_cbillnum_cprefix on table aa_billprefix
--

--
-- ALTER TABLE aa_billprefix
--  ADD INDEX ix_aa_billprefix_cbillnum_cprefix(cbillnum, cprefix, orgId);
ALTER TABLE aa_billprefix ADD INDEX ix_aa_billprefix_cbillnum_tenantId(cbillnum, yhtTenantId);
ALTER TABLE aa_billprefix ADD INDEX ix_aa_billprefix_bnCode_orgId_tenantId(billnumberCode, orgId, yhtTenantId);

--
-- Create table aa_billprefabricate
--
CREATE TABLE aa_billprefabricate (
  autoid INT(11) NOT NULL AUTO_INCREMENT,
  cbillnum VARCHAR(50) NOT NULL COMMENT '表单编码',
  cprefix VARCHAR(50) NOT NULL COMMENT '前缀名字',
  cprefixid VARCHAR(50) NOT NULL COMMENT '唯一uid',
  cprefixtype INT(11) NOT NULL COMMENT '前缀类型 0=字符串 1=date 2=文本',
  iprefixtype TINYINT(4) DEFAULT NULL COMMENT '前缀类型 0=字符串 1=date 2=文本',
  ipurpose TINYINT(4) DEFAULT NULL COMMENT '是否取值，设置为0',
  csourcename VARCHAR(50) DEFAULT NULL  COMMENT '元数据实体uri，billitem_base里的cdatasourceName',
  cfieldname VARCHAR(50) DEFAULT NULL COMMENT '取值字段',
  carchname VARCHAR(50) DEFAULT NULL,
  carchfieldname VARCHAR(50) DEFAULT NULL,
  carchclsfieldname VARCHAR(50) DEFAULT NULL,
  ckeyword VARCHAR(50) DEFAULT NULL,
  ckeywordnamefield VARCHAR(50) DEFAULT NULL,
  tenant_id BIGINT(20) DEFAULT NULL COMMENT '租户',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户',
  bMain TINYINT(4) NOT NULL DEFAULT '1' COMMENT '是否主表字段，0否 1是',
  refPropType tinyint(4) DEFAULT NULL COMMENT '属性为参照时，值类型：0=id，1=code，2=name',
  refDomain VARCHAR(255) DEFAULT NULL COMMENT '参照领域',
  refCode VARCHAR(255) DEFAULT NULL COMMENT '参照编码',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 17667,
AVG_ROW_LENGTH = 4096,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '预置表单',
ROW_FORMAT = COMPACT;

--
-- Create index ix_aa_billprefabricate_cbillnum_cprefix on table aa_billprefabricate
--
ALTER TABLE aa_billprefabricate
  ADD INDEX ix_aa_billprefabricate_cbillnum_cprefix(cbillnum, cprefix);

--
-- Create table aa_billprecode
--
CREATE TABLE aa_billprecode (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  tenant_id BIGINT(20) DEFAULT 0 COMMENT '租户',
  orgId VARCHAR(64) NOT NULL DEFAULT -1 COMMENT '组织',
  cbillnum VARCHAR(50) DEFAULT NULL COMMENT '表单编码',
  ownerorg BIGINT(20) DEFAULT NULL COMMENT '所属组织',
  cglide VARCHAR(250) DEFAULT NULL COMMENT '前缀名称',
  cgliderule VARCHAR(250) DEFAULT NULL COMMENT '取值规则',
  cseed VARCHAR(250) NOT NULL COMMENT '编号前缀',
  inumber INT(11) NOT NULL COMMENT '流水号',
  totalBasis VARCHAR(500) NOT NULL COMMENT '总依据(依据1|依据2|依据3|...)',
  billnumberid INT(11) NOT NULL COMMENT '规则id（外键）',
  billcode VARCHAR(250) DEFAULT NULL COMMENT '编码号',
  dr TINYINT(1) DEFAULT NULL COMMENT '删除标记',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间戳',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '预取表',
ROW_FORMAT = COMPACT;

--
-- Create table aa_billnumber
--
CREATE TABLE aa_billnumber (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  tenant_id BIGINT(20) DEFAULT 0 COMMENT '租户',
  orgId VARCHAR(64) NOT NULL DEFAULT -1 COMMENT '组织',
  cbillnum VARCHAR(50) NOT NULL COMMENT '作为规则编码，表单模式下是表单编码，元数据模式下是元数据ID，自定义实体模式下是自定义实体主键ID',
  cbillname VARCHAR(50) NOT NULL COMMENT '表单名称',
  csubid VARCHAR(20) DEFAULT NULL  COMMENT '使用bill_base表中的cSub_id字段',
  ballowhandwork TINYINT(4) NOT NULL COMMENT '允许手动编码',
  brepeatredo TINYINT(4) NOT NULL COMMENT '允许手工可改',
  istartnumber INT(11) NOT NULL COMMENT '流水号初始值',
  iseriallen TINYINT(4) NOT NULL COMMENT '流水号长度',
  billnumLen INT(11) NOT NULL DEFAULT 8 COMMENT '流水号长度',
  billnumInit INT(11) NOT NULL DEFAULT 1 COMMENT '流水号初始值',
  billnumTruncatType INT(11) NOT NULL DEFAULT 0 COMMENT '截断类型0 = 左截断 1 = 右截断',
  billnumFillType INT(11) NOT NULL DEFAULT 0 COMMENT '补位类型0=不补位 1=左补位 2=右补位',
  billnumFillMark VARCHAR(20) NOT NULL DEFAULT '0' COMMENT '补位符',
  billnumMode INT(11) NOT NULL DEFAULT 0 COMMENT '0=手工编号 1=自动编号 2=自动编号 手工可改',
  billnumRule INT(11) NOT NULL DEFAULT 0 COMMENT '使用规则 0 企业默认 1 自定义规则',
  isReuse TINYINT(1) DEFAULT 0 COMMENT '是否开启退号补号',
  sysid VARCHAR(40) NOT NULL COMMENT '系统ID',
  datatype TINYINT(4) NOT NULL DEFAULT 2 COMMENT '编码实体类型，1：表单，2：元数据，3：自定义实体',
  rulecode VARCHAR(100) NOT NULL COMMENT '规则编码',
  rulename VARCHAR(255) DEFAULT NULL COMMENT '规则名称',
  dr TINYINT(4) NOT NULL DEFAULT 0 COMMENT '删除标记',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间戳',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户',
  sntype TINYINT(4) NOT NULL DEFAULT 0  COMMENT '流水类型，是否首字母流水',
  isDefault TINYINT(1) NOT NULL DEFAULT '1' COMMENT '是否默认，0否 1是',
  code VARCHAR(50) NOT NULL COMMENT '规则code',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 47,
AVG_ROW_LENGTH = 496,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '编码规则-主表',
ROW_FORMAT = COMPACT;

--
-- Create index ix_aa_billnumber_cbillnum on table aa_billnumber
--
ALTER TABLE aa_billnumber
  ADD INDEX ix_aa_billnumber_cbillnum(cbillnum);

--
-- Create index ix_aa_billnumber_cbillnum_orgid on table aa_billnumber
--

-- ALTER TABLE aa_billnumber
--  ADD UNIQUE INDEX ix_aa_billnumber_cbillnum_orgid(cbillnum, orgId, tenant_id);
--
ALTER TABLE aa_billnumber ADD INDEX ix_aa_billnumber_cbillnum_orgid_tenantId(cbillnum, orgId, yhtTenantId);
ALTER TABLE aa_billnumber ADD UNIQUE INDEX ix_aa_billnumber_code_orgId_tenantId(code, orgId, yhtTenantId);

--
-- Create index ix_aa_billnumber_rulecode on table aa_billnumber
--
ALTER TABLE aa_billnumber
  ADD INDEX ix_aa_billnumber_rulecode(rulecode);

--
-- Create table aa_billhistory
--
CREATE TABLE aa_billhistory (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  tenant_id BIGINT(20) DEFAULT 0 COMMENT '租户',
  orgId VARCHAR(64) NOT NULL DEFAULT -1 COMMENT '组织',
  cbillnum VARCHAR(50) NOT NULL COMMENT '表单编码',
  ownerorg BIGINT(20) DEFAULT NULL COMMENT '所属组织',
  cglide VARCHAR(100) DEFAULT NULL COMMENT '前缀名称',
  cgliderule VARCHAR(100) DEFAULT NULL COMMENT '取值规则',
  cseed VARCHAR(100) NOT NULL COMMENT '编号前缀',
  inumber INT(11) NOT NULL COMMENT '流水号',
  totalBasis VARCHAR(100) NOT NULL COMMENT '总依据(依据1|依据2|依据3|...)',
  billnumberid INT(11) DEFAULT NULL COMMENT '规则id（外键）',
  dr TINYINT(1) DEFAULT NULL COMMENT '删除标记',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '时间戳',
  yhtTenantId VARCHAR(36) DEFAULT NULL COMMENT '友户通租户Id',
  billnumberCode VARCHAR(50) NOT NULL COMMENT '规则code',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 5580,
AVG_ROW_LENGTH = 606,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '流水号',
ROW_FORMAT = COMPACT;

--
-- Create index cbillnum on table aa_billhistory
--
ALTER TABLE aa_billhistory
  ADD INDEX cbillnum(yhtTenantId, cbillnum, cseed);

--
-- Create index ix_aa_billhistory_cbillnum_cglide on table aa_billhistory
--
ALTER TABLE aa_billhistory
  ADD INDEX ix_aa_billhistory_cbillnum_cglide(cbillnum, cglide);

ALTER TABLE aa_billhistory
  ADD INDEX ix_aa_billhistory_cbn_bnCd_org_tenantId(cbillnum, billnumberCode, orgId, yhtTenantId);

--
-- Create table aa_billcode_obj
--
CREATE TABLE aa_billcode_obj (
  pk_billobj VARCHAR(40) NOT NULL COMMENT '主键',
  code VARCHAR(40) DEFAULT NULL COMMENT '编码',
  name VARCHAR(40) DEFAULT NULL COMMENT '名称',
  name2 VARCHAR(40) DEFAULT NULL COMMENT '多语名称',
  name3 VARCHAR(40) DEFAULT NULL COMMENT '多语名称',
  name4 VARCHAR(40) DEFAULT NULL COMMENT '多语名称',
  name5 VARCHAR(40) DEFAULT NULL COMMENT '多语名称',
  name6 VARCHAR(40) DEFAULT NULL COMMENT '多语名称',
  name_ext VARCHAR(40) DEFAULT NULL COMMENT '扩展名称',
  createdate TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  tenantid VARCHAR(64) DEFAULT NULL COMMENT '租户id，当租户值为''system''时，表示该实体为系统级',
  sysid VARCHAR(64) DEFAULT NULL COMMENT '系统id',
  classify_code VARCHAR(64) DEFAULT NULL COMMENT '该编码实体所属的二级分类树code',
  service_code VARCHAR(64) DEFAULT NULL COMMENT '关联diwork的原子服务code',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (pk_billobj)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 103,
CHARACTER SET utf8,
COLLATE utf8_general_ci,
ROW_FORMAT = COMPACT;

--
-- Create table aa_billcode_mappingchild
--
CREATE TABLE aa_billcode_mappingchild (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  pk_map_id INT(11) DEFAULT NULL COMMENT '映射主表主键值',
  pk_ref VARCHAR(200) DEFAULT NULL COMMENT '属性值（参照属性主键值）',
  map_val VARCHAR(200) DEFAULT NULL COMMENT '编码',
  ref_code VARCHAR(255) DEFAULT NULL COMMENT '参照编码',
  ref_name VARCHAR(255) DEFAULT NULL COMMENT '参照名称',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  ref_code_display VARCHAR(200) DEFAULT NULL COMMENT '参照显示名称',
  tenantid VARCHAR(200) DEFAULT NULL COMMENT '租户id',
  sysid VARCHAR(200) DEFAULT NULL COMMENT '系统id',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 132,
AVG_ROW_LENGTH = 252,
CHARACTER SET utf8,
COLLATE utf8_general_ci,
ROW_FORMAT = COMPACT;

--
-- Create table aa_billcode_mapping
--
CREATE TABLE aa_billcode_mapping (
  autoid INT(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  name VARCHAR(200) DEFAULT NULL COMMENT '名称',
  domain VARCHAR(200) NOT NULL COMMENT '应用编码',
  ref_code VARCHAR(200) NOT NULL COMMENT '参照编码',
  md_en_id VARCHAR(200) NOT NULL COMMENT '元数据实体ID',
  map_length INT(11) NOT NULL COMMENT '映射值长度',
  fill_style TINYINT(1) NOT NULL DEFAULT 0 COMMENT '补位方式，0不补位 1左补位 2右补位',
  fill_char VARCHAR(1) DEFAULT NULL COMMENT '补位符号',
  map_prop_type TINYINT(1) NOT NULL DEFAULT 0 COMMENT '映射的值是mappingchild中的pk，code还是name，pk为0 code为1  name为2',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  tenantid VARCHAR(255) NOT NULL COMMENT '租户id',
  sysid VARCHAR(255) DEFAULT NULL COMMENT '系统id',
  default_map_value VARCHAR(255) DEFAULT NULL COMMENT '默认映射值',
  label VARCHAR(255) DEFAULT NULL COMMENT '标签 (为了处理组织类的元数据通过标签来匹配)',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
AUTO_INCREMENT = 20,
AVG_ROW_LENGTH = 1820,
CHARACTER SET utf8,
COLLATE utf8_general_ci,
COMMENT = '编码映射主表';

--
-- Create table aa_billcode_candidateprop
--
CREATE TABLE IF NOT EXISTS `aa_billcode_candidateprop` (
  `autoid` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `pk_bcr_obj` varchar(200) DEFAULT NULL COMMENT '编码对象主键',
  `en_prop_name` varchar(200) DEFAULT NULL COMMENT '实体属性名',
  `display_name` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `elem_type` varchar(200) DEFAULT NULL COMMENT '类型（时间，字符，参照）',
  `mapping_entity` varchar(200) DEFAULT NULL COMMENT '所属编码映射实体',
  `pubts` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `mapping_entity_name` varchar(200) DEFAULT NULL COMMENT '引用实体名称',
  `display_name2` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `display_name3` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `display_name4` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `display_name5` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `display_name6` varchar(200) DEFAULT NULL COMMENT '显示名称',
  `display_name_ext` varchar(200) DEFAULT NULL COMMENT '显示名称',
  PRIMARY KEY (`autoid`)
)
ENGINE = INNODB,
AVG_ROW_LENGTH = 1260,
CHARACTER SET utf8,
COLLATE utf8_general_ci,
COMMENT = '候选属性表',
ROW_FORMAT = COMPACT;


-- ----------------------------
-- Table structure for aa_billcondition
-- ----------------------------
DROP TABLE IF EXISTS aa_billcondition;
CREATE TABLE IF NOT EXISTS aa_billcondition (
  autoid int(11) NOT NULL AUTO_INCREMENT COMMENT '主键 数据库自增',
  cbillnum varchar(64) NOT NULL COMMENT '单据billNum',
  ccondition varchar(255) NOT NULL COMMENT '条件字段名称',
  cconditionresid varchar(255) DEFAULT NULL COMMENT '多语资源ID',
  cconditionid varchar(64) DEFAULT NULL COMMENT '条件ID aa_billprefabricate表预制数据的UUID',
  cconditiontype int(11) NOT NULL COMMENT '条件类型 0=字符串 1=日期 2=数字',
  cconditionvalue TEXT DEFAULT NULL COMMENT '条件值',
  csourcename varchar(255) DEFAULT NULL COMMENT '即datasourceName 取aa_billprefabricate表里的cdatasourceName',
  cfieldname varchar(255) NOT NULL COMMENT '即cFieldName 取aa_billprefabricate表里的cFieldName',
  bMain int(11) DEFAULT 1 COMMENT '是否主表字段 0=否 1=是',
  billnumberid int(11) DEFAULT NULL COMMENT '编码规则主键 编码规则主键',
  billnumberCode varchar(50) NOT NULL COMMENT '规则编码 关联编码规则主表',
  orgId varchar(64) NOT NULL COMMENT '组织ID',
  tenant_id bigint(20) DEFAULT NULL COMMENT '租户 系统租户=0',
  yhtTenantId varchar(36) DEFAULT NULL COMMENT '友户通租户',
  logical varchar(10) DEFAULT NULL COMMENT '逻辑符 &&、||',
  operator varchar(10) NOT NULL COMMENT '操作符 ==、<、>、<=、>=',
  formula varchar(500) DEFAULT NULL COMMENT '公式',
  formuladisplay varchar(500) DEFAULT NULL COMMENT '公式显示',
  iorder int(11) NOT NULL COMMENT '顺序 条件顺序',
  dr int(11) DEFAULT NULL COMMENT '删除标记 1=已删除，0=未删除',
  pubts TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '时间戳 当前时间',
  refPropType tinyint(4) DEFAULT NULL COMMENT '属性为参照时，值类型：0=id，1=code，2=name',
  refDisplayName varchar(255) DEFAULT NULL COMMENT '参照显示名称',
  cmdId VARCHAR(255) DEFAULT NULL COMMENT '参照实体元数据id',
  refId TEXT DEFAULT NULL COMMENT '参照id',
  refCode TEXT DEFAULT NULL COMMENT '参照编码',
  refName TEXT DEFAULT NULL COMMENT '参照名称',
  refDataCode TEXT DEFAULT NULL COMMENT '参照数据编码',
  ytenant_id VARCHAR(36) DEFAULT '~' COMMENT '租户',
  PRIMARY KEY (autoid)
)
ENGINE = INNODB,
CHARACTER SET utf8mb4,
COLLATE utf8mb4_general_ci,
COMMENT = '条件数据表',
ROW_FORMAT = COMPACT;

-- ALTER TABLE aa_billcondition ADD INDEX ix_aa_billcondition_cbillnum_tenantId(cbillnum, tenant_id);
ALTER TABLE aa_billcondition ADD INDEX ix_aa_billcondition_cbillnum_yhtTenantId(cbillnum, yhtTenantId);

ALTER TABLE aa_billprefix ADD COLUMN isvTag varchar(64) DEFAULT NULL COMMENT 'isv标志';
ALTER TABLE aa_billnumber ADD COLUMN isvTag varchar(64) DEFAULT NULL COMMENT 'isv标志';
ALTER TABLE aa_billprefabricate ADD COLUMN isvTag varchar(64) DEFAULT NULL COMMENT 'isv标志';
ALTER TABLE aa_billcode_mapping ADD COLUMN isvTag varchar(64) DEFAULT NULL COMMENT 'isv标志';