//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

package com.yonyou.diwork.multilingual.impl;

import com.yonyou.cloud.mwclient.MwClientStartUp;
import com.yonyou.diwork.multilingual.service.ILanguageService;
import com.yonyou.iuap.context.ContextHolder;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.iuap.formula.common.SpringContextHolder;
import com.yonyou.iuap.ml.provider.IMultiLangProvider;
import com.yonyou.iuap.ml.vo.LanguageVO;
import com.yonyou.iuap.ucf.common.ml.DefaultMultiLangProvider;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DiworkMultLangProviderImpl implements IMultiLangProvider {
    private Logger logger = LoggerFactory.getLogger(DiworkMultLangProviderImpl.class);
    private IMultiLangProvider defaultProvider;
    private boolean isEnableIrisRpc;
    //    @Autowired(required = false)
    private ILanguageService languageService;

//    @Autowired
//    public void setLanguageService() {
//        this.languageService =
//    }

    public DiworkMultLangProviderImpl() {
        boolean hasIrisClass = false;

        try {
            hasIrisClass = null != Class.forName("com.yonyou.cloud.mwclient.MwClientStartUp", false, DiworkMultLangProviderImpl.class.getClassLoader());
        } catch (ClassNotFoundException var4) {
        }

        this.isEnableIrisRpc = hasIrisClass && MwClientStartUp.getEnableConfig();

        try {
            Class<?> forName = Class.forName("com.yonyou.iuap.ucf.common.ml.XmlMultiLangProvider");
            this.defaultProvider = (IMultiLangProvider) forName.newInstance();
        } catch (IllegalAccessException | ClassNotFoundException | InstantiationException var3) {
            this.defaultProvider = new DefaultMultiLangProvider();
        }

    }

    public LinkedList<LanguageVO> getAllLangVos() {
        this.validate();
        String tenantId = InvocationInfoProxy.getTenantid();
        if (!StringUtils.isBlank(tenantId) && !"null".equals(tenantId)) {
//            List<com.yonyou.diwork.multilingual.model.LanguageVO> lvs = Lists.newArrayList(this.getLanguageService().findAll(tenantId));
            List<com.yonyou.diwork.multilingual.model.LanguageVO> lvs = new ArrayList<>();
            if (((List) lvs).isEmpty()) {
                this.logger.error("在当前租户下未查到可用语种，请检查环境配置是否正确。当前租户id={},返回默认语种列表", tenantId);
                lvs = (List) ((List) com.yonyou.diwork.multilingual.model.LanguageVO.DEFAULT_LIST.get("diwork")).stream().filter((oo) -> {
//                    return oo.isDefault();
                    return true;
                }).collect(Collectors.toList());
            }

            LinkedList<LanguageVO> result = new LinkedList();
            ((List) lvs).forEach((lv) -> {
                LanguageVO temp = new LanguageVO();
                BeanUtils.copyProperties(lv, temp);
                result.add(temp);
            });
            return result;
        } else {
            this.logger.info("当前上下文租户id为空(InvocationInfoProxy.getTenantid() == null)，取默认语种:" + this.defaultProvider.getClass().getName());
            if ("null".equals(tenantId)) {
                this.logger.error("InvocationInfoProxy 中租户 id 为字符串 \"null\"，请检查租户 id 的上下文传递，保证 iris-iuap-support 版本 >= 5.2.1-RELEASE");
            }

            return this.defaultProvider.getEnableLangVOs();
        }
    }

    public LinkedList<LanguageVO> getEnableLangVOs() {
        LinkedList<LanguageVO> result = new LinkedList();
        this.getAllLangVos().forEach((temp) -> {
            if (temp.isEnabled() || temp.isDefault()) {
                result.add(temp);
            }

        });
        return result;
    }

    public LanguageVO getDefaultLangVO() {
        List<LanguageVO> temps = this.getEnableLangVOs();
        Iterator var2 = temps.iterator();

        LanguageVO temp;
        do {
            if (!var2.hasNext()) {
                this.logger.error("tenantId={}未找到默认语言", InvocationInfoProxy.getTenantid());
                throw new RuntimeException("tenantId=" + InvocationInfoProxy.getTenantid() + "未找到默认语言");
            }

            temp = (LanguageVO) var2.next();
        } while (!temp.isDefault());

        return temp;
    }

    private void validate() {
        if (!this.isEnableIrisRpc || this.languageService == null && ContextHolder.getContext() == null) {
            StringBuilder errMsg = new StringBuilder("多语实现类未正确配置:");
            if (!this.isEnableIrisRpc) {
                errMsg.append("    您尚未启用IRIS RPC，无法使用DiworkMultLangProviderImpl;\n");
            }

            if (ContextHolder.getContext() == null) {
                errMsg.append("    您尚未配置com.yonyou.iuap.context.ContextHolder.getContext,无法使用DiworkMultLangProviderImpl;\n");
            }

            throw new IllegalStateException(errMsg.toString());
        }
    }

    private ILanguageService getLanguageService() {
        if (this.languageService == null) {
            this.languageService = SpringContextHolder.getBean(ILanguageService.class);
        }

        return this.languageService;
    }
}
