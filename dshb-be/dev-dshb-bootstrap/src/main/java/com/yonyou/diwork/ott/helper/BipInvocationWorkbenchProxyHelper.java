//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.yonyou.diwork.ott.helper;

import com.yonyou.cloud.middleware.framework.IrisSession;
import com.yonyou.diwork.config.DiworkRobotProperties;
import com.yonyou.diwork.exception.DiworkRuntimeException;
import com.yonyou.diwork.ott.RobotInvocationGetter;
import com.yonyou.diwork.ott.util.BipInvocationDecoder;
import java.util.Map;

import com.yonyou.ucf.mdd.ext.core.AppContext;
import com.yonyou.ucf.mdd.ext.dubbo.DubboReferenceUtils;
import com.yonyou.ucf.mdd.isv.service.ISVServiceFactory;
import com.yonyou.ucf.transtype.service.itf.ITransTypeService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BipInvocationWorkbenchProxyHelper {
    private static Logger logger = LoggerFactory.getLogger(BipInvocationWorkbenchProxyHelper.class);
    private static RobotInvocationGetter robotInvocationGetter;
    private static final String ROBOTDOMAIN = "workbench-service";

    public BipInvocationWorkbenchProxyHelper() {
    }

    public static Map<String, String> getFromWorkbench(String tenantId, String userId) {
        RobotInvocationGetter robotService = DubboReferenceUtils.getDubboService(RobotInvocationGetter.class, ROBOTDOMAIN, null);
        robotInvocationGetter = robotService;
        if (robotInvocationGetter == null) {
            logger.error("spring/springboot中未获取到RobotInvocationGetter bean,请检查是否集成了iris以及包扫描范围com.yonyou.diwork.ott");
            throw new DiworkRuntimeException("spring/springboot中未获取到RobotInvocationGetter bean,请检查是否集成了iris以及包扫描范围com.yonyou.diwork.ott");
        } else {
            String result;
            if (StringUtils.isNotBlank(DiworkRobotProperties.workbenchRpcVersion)) {
                try {
                    result = (String)IrisSession.execute(() -> {
                        return robotInvocationGetter.get(tenantId, userId);
                    }, DiworkRobotProperties.workbenchRpcVersion, DiworkRobotProperties.workbenchRpcForce, DiworkRobotProperties.rpcTimeout);
                } catch (Throwable var4) {
                    if (var4 instanceof DiworkRuntimeException) {
                        throw (DiworkRuntimeException)var4;
                    }

                    throw new DiworkRuntimeException(var4);
                }
            } else {
                result = robotInvocationGetter.get(tenantId, userId);
            }

            return BipInvocationDecoder.decode(result);
        }
    }

    @Autowired(required = false)
    private void setRobotInvocationGetter(RobotInvocationGetter robotInvocationGetter) {
        BipInvocationWorkbenchProxyHelper.robotInvocationGetter = robotInvocationGetter;
    }

    public static void setRobotInvocationGetter4NSpring(RobotInvocationGetter getter) {
        robotInvocationGetter = getter;
    }
}
