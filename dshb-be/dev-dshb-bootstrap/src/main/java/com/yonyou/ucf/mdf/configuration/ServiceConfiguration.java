package com.yonyou.ucf.mdf.configuration;

import com.yonyou.iuap.billcode.service.IBillNumberLogService;
import com.yonyou.iuap.billcode.service.impl.BillNumberLogServiceImpl;
import com.yonyou.iuap.context.ContextHolder;
import com.yonyou.iuap.elasticdb.sql.LogicTableGeneratorAPI;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.LogicTableGeneratorImpl;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.WideTableMapper;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.dao.WideTableDao;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.ElasticObjectMapper;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.ParallelMetaStrategyDetailMapper;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.ParallelMetaStrategyMapper;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.impl.ElasticObjectMapperImpl;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.impl.ParallelMetaStrategyDetailMapperImpl;
import com.yonyou.iuap.elasticdb.sql.implementation.mybatics.mapper.impl.ParallelMetaStrategyMapperImpl;
import com.yonyou.iuap.ml.provider.IMultiLangProvider;
import com.yonyou.iuap.ucf.common.ml.DefaultMultiLangProvider;
import com.yonyou.ucf.mdd.common.context.impl.MddEnvServiceImpl;
import com.yonyou.ucf.mdd.ext.bill.barcode.service.BarCodeGenerator;
import com.yonyou.ucf.mdd.ext.bill.barcode.service.DefaultBarCodeGenerator;
import com.yonyou.ucf.mdd.ext.bill.biz.ImportLimit;
import com.yonyou.ucf.mdd.ext.bill.dao.StateRuleDAOImpl;
import com.yonyou.ucf.mdd.ext.bill.meta.service.EnumDataService;
import com.yonyou.ucf.mdd.ext.bill.meta.service.RefDataService;
import com.yonyou.ucf.mdd.ext.bill.service.*;
import com.yonyou.ucf.mdd.ext.bpm.service.ProcessService;
import com.yonyou.ucf.mdd.ext.datatransfer.IBillFormatService;
import com.yonyou.ucf.mdd.ext.dubbo.ExtDubboReference;
import com.yonyou.ucf.mdd.ext.i18n.service.IMddMultiLangEnumService;
import com.yonyou.ucf.mdd.ext.i18n.service.IMddMultiLangFilterService;
import com.yonyou.ucf.mdd.ext.i18n.service.IMddMultiLangRefService;
import com.yonyou.ucf.mdd.ext.i18n.service.MddMultiLangBaseServiceImpl;
import com.yonyou.ucf.mdd.ext.i18n.service.impl.MddMultiLangEnumServiceImpl;
import com.yonyou.ucf.mdd.ext.i18n.service.impl.MddMultiLangFilterServiceImpl;
import com.yonyou.ucf.mdd.ext.i18n.service.impl.MddMultiLangRefServiceImpl;
import com.yonyou.ucf.mdd.ext.itf.IExtProxyService;
import com.yonyou.ucf.mdd.ext.itf.IUserOperationService;
import com.yonyou.ucf.mdd.ext.poi.common.PoiInitService;
import com.yonyou.ucf.mdd.ext.poi.common.StartPoiInitService;
import com.yonyou.ucf.mdd.ext.poi.exportbiz.service.impl.EnumDataServiceImpl;
import com.yonyou.ucf.mdd.ext.poi.exportbiz.service.impl.RefDataServiceImpl;
import com.yonyou.ucf.mdd.ext.poi.importbiz.init.ImportServiceInit;
import com.yonyou.ucf.mdd.ext.poi.importbiz.service.ImportSingleService;
import com.yonyou.ucf.mdd.ext.poi.importbiz.service.handle.ImportDataTypeCheckHandler;
import com.yonyou.ucf.mdd.ext.poi.importbiz.service.handle.ImportPrivilegeCheckHandler;
import com.yonyou.ucf.mdd.ext.poi.service.POIService;
import com.yonyou.ucf.mdd.ext.proxy.ExtProxyService;
import com.yonyou.ucf.mdd.ext.report.service.*;
import com.yonyou.ucf.mdd.ext.service.DefaultBillService;
import com.yonyou.ucf.mdd.ext.sys.auth.AuthCache;
import com.yonyou.ucf.mdd.ext.sys.service.MenuService;
import com.yonyou.ucf.mdd.ext.sys.service.UserOperationService;
import com.yonyou.ucf.mdf.core.uitemplate.service.MdfTemplateRunTimeService;
import com.yonyou.ucf.mdf.core.uitemplate.service.impl.MdfTemplateRunTimeServiceImpl;
import com.yonyoucloud.uretail.sys.itf.IAuthCache;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * FileName: ServiceConfiguration
 * Author: WP
 * Date: 2020/9/2 10:12
 * Description:
 * History:
 **/
@Configuration
public class ServiceConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public ExtDubboReference extDubboReference(){
        return new ExtDubboReference();
    }

    @Bean
    @ConditionalOnMissingBean
    public ContextHolder contextHolder(){
        return new ContextHolder();
    }

  /*  @Bean
    @ConditionalOnMissingBean
    public EnvironmentHelper environmentHelper() {
        return new EnvironmentHelper();
    }*/

    @Bean
    @ConditionalOnMissingBean
    public DefaultBillService defaultBillService() {
        return new DefaultBillService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IMddMultiLangRefService mddMultiLangRefServiceImpl() {
        return new MddMultiLangRefServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public MdfTemplateRunTimeService MdfTemplateRunTimeService() {
        return new MdfTemplateRunTimeServiceImpl();
    }
//    @Bean
//    @ConditionalOnMissingBean
//    public RefService refService() {
//        return new RefService();
//    }

    @Bean

    @ConditionalOnMissingBean
    public IMultiLangProvider multiLangProvider() {
        return new DefaultMultiLangProvider();
    }

//    @Bean({"extendCache", "defaultCache"})
//    @ConditionalOnMissingBean
//    public IMddCacheService extendCache() {
//        return new MddPubCacheServiceImpl();
//    }

//    @Bean
//    @ConditionalOnMissingBean
//    public MddSqlSessionServiceImpl mddSqlSessionServiceImpl() {
//        return new MddSqlSessionServiceImpl();
//    }

    @Bean
    @ConditionalOnMissingBean
    public ImportLimit mddImportLimit() {
        return ImportLimit.getInstance();
    }

    @Bean
    @ConditionalOnMissingBean
    public MddEnvServiceImpl mddEnvService() {
        return new MddEnvServiceImpl();
    }


    @Bean
    @Primary
    public MddMultiLangBaseServiceImpl mddMultiLangBaseServiceImpl() {
        return new MddMultiLangBaseServiceImpl();
    }

//    @Bean
//    public MddMultiLangMetaDaoServiceImpl mddMultiLangMetaDaoService() {
//        return new MddMultiLangMetaDaoServiceImpl();
//    }

/*    @Bean
    @ConditionalOnMissingBean
    public ImportSingleService MddImportSingleService() {
        return new ImportSingleService();
    }*/

    @Bean
    @ConditionalOnMissingBean
    public ImportPrivilegeCheckHandler importPrivilegeCheckHandler() {
        return new ImportPrivilegeCheckHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public ImportDataTypeCheckHandler importDataTypeCheckHandler() {
        return new ImportDataTypeCheckHandler();
    }

    @Bean
    @ConditionalOnMissingBean
    public IMddMultiLangEnumService iMddMultiLangEnumService() {
        return new MddMultiLangEnumServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public IMddMultiLangFilterService mddMultiLangFilterServiceImpl(){
        return new MddMultiLangFilterServiceImpl();
    }


/*    @Bean
    @ConditionalOnMissingBean
    @Primary
    public WebApiInvokeService WebApiInvokeServiceImpl(){
        return new WebApiInvokeServiceImpl();
    }*/

    @Bean
    @ConditionalOnMissingBean
    protected MenuService menuService(){
        return new MenuService();
    }

    @Bean
    @ConditionalOnMissingBean
    public POIService POIService(){
        return new POIService();
    }

    @Bean
    @ConditionalOnMissingBean
    public StateRuleService stateRuleService(){
        return new StateRuleService();
    }

    @Bean
    @ConditionalOnMissingBean
    public StateRuleDAOImpl stateRuleDAO(){
        return new StateRuleDAOImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public EnumDataService enumDataService(){
        return new EnumDataServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public RefDataService refDataService(){
        return new RefDataServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public IGroupSchemaService groupSchemaService(){
        return new GroupSchemaService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IReportViewService reportViewService(){
        return new ReportViewService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IReportSubscriptionService reportSubscriptionService(){
        return new ReportSubscriptionService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IUserOperationService userOperationService(){
        return new UserOperationService();
    }

    @Bean
    @ConditionalOnMissingBean
    public IAuthCache iAuthCache(){
        return new AuthCache();
    }

    @Bean
    @ConditionalOnMissingBean
    public ProcessService processService(){
        return new ProcessService();
    }

    @Bean
    @ConditionalOnMissingBean
    public SaveBillService saveBillService(){
        return new SaveBillServiceImpl();
    }

    /*@Bean
    @ConditionalOnMissingBean
    public SaveTimelineService saveTimelineService(){
        return new SaveTimelineServiceImpl();
    }*/

    @Bean
    @ConditionalOnMissingBean
    public BarCodeGenerator barCodeGenerator(){
        return new DefaultBarCodeGenerator();
    }

    @Bean
    @ConditionalOnMissingBean
    public IBillFormatService billFormatServiceImpl(){
        return new BillFormatServiceImpl();
    }

    @Bean
    @ConditionalOnMissingBean
    public PoiInitService importServiceInit(){
        return new ImportServiceInit();
    }

    @Bean
    @ConditionalOnMissingBean
    @Primary
    public StartPoiInitService startPoiInitService(){
        return new StartPoiInitService();
    }

    /*@Bean
    @ConditionalOnMissingBean
    public IMakeBillDataService makeBillDataService(){
        return new MakeBillDataServiceImpl();
    }*/

    @Bean
    @ConditionalOnMissingBean
    public IExtProxyService extProxyService(){
        ExtProxyService extProxyService = new ExtProxyService();
        return extProxyService;
    }

    /* @Bean
     @ConditionalOnMissingBean
     public BuildFlowDataRuleOld buildFlowDataRuleOld(){

         return new BuildFlowDataRuleOld();
     }*/
    @Bean
    @ConditionalOnMissingBean
    public IBillNumberLogService billNumberLogService(){
        IBillNumberLogService billNumberLogService = new BillNumberLogServiceImpl();
        return billNumberLogService;
    }

//    @Bean
//    @ConditionalOnMissingBean
//    public LogicTableGeneratorAPI logicTableGeneratorAPI(){
//        LogicTableGeneratorAPI logicTableGeneratorAPI = new LogicTableGeneratorImpl();
//        return logicTableGeneratorAPI;
//    }

    @Bean
    @ConditionalOnMissingBean
    public WideTableDao wideTableDao(){
        WideTableDao wideTableDao = new WideTableMapper();
        return wideTableDao;
    }

    @Bean
    @ConditionalOnMissingBean
    public ParallelMetaStrategyMapper parallpelMetaStrategyMapperImpl(){
        ParallelMetaStrategyMapper parallpelMetaStrategyMapperImpl = new ParallelMetaStrategyMapperImpl();
        return parallpelMetaStrategyMapperImpl;
    }

    @Bean
    @ConditionalOnMissingBean
    public ElasticObjectMapper elasticObjectMapper(){
        ElasticObjectMapper elasticObjectMapper = new ElasticObjectMapperImpl();
        return elasticObjectMapper;
    }

    @Bean
    @ConditionalOnMissingBean
    public ParallelMetaStrategyDetailMapper parallelMetaStrategyDetailMapper(){
        ParallelMetaStrategyDetailMapper parallelMetaStrategyDetailMapper = new ParallelMetaStrategyDetailMapperImpl();
        return parallelMetaStrategyDetailMapper;
    }
    @Bean
    @ConditionalOnMissingBean
    public IBillNumberLogService billNumberLogServiceImpl() {
        return new BillNumberLogServiceImpl();
    }

}
