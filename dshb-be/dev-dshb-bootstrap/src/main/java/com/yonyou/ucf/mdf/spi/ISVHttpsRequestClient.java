package com.yonyou.ucf.mdf.spi;

import com.alibaba.fastjson.JSON;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.ucf.mdd.core.http.HttpsRequestClientSpi;
import com.yonyou.ucf.mdd.core.http.ResultEntity;
import com.yonyou.ucf.mdd.ext.core.AppContext;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequest;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequestBody;
import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

@Slf4j
public class ISVHttpsRequestClient implements HttpsRequestClientSpi {

  private static final String GET = "GET";
  private static final String POST = "POST";

  private static ISVRequest isvRequest = AppContext.getBean(ISVRequest.class);

  @Override
  public CloseableHttpResponse doGet(CloseableHttpClient httpClient,
      Map<String, Object> headerParams, HttpGet httpGet, ResultEntity resultEntity)
      throws IOException {
    URI uri = httpGet.getURI();
    String url = uri.toURL().toString();
    ISVRequestBody isvRequestBody = new ISVRequestBody();
    isvRequestBody.setRequestMethod(GET);
    isvRequestBody.setUrl(url);
    HashMap<String,String> headers = new HashMap<>();
    if (headerParams != null) {
      Set<String> headerKeys = headerParams.keySet();
      for (String key : headerKeys) {
        headers.put(key, headerParams.get(key).toString());
      }
    }
    Header[] allHeaders = httpGet.getAllHeaders();
    Arrays.stream(allHeaders).forEach(header -> {
      headers.put(header.getName(),header.getValue());
    });
    isvRequestBody.setHeaders(headers);
    isvRequestBody.setYhtAccessToken("yht_access_token_encryption");
    // 获取结果实体
    CloseableHttpResponse httpResponse = isvRequest.doCloseableHttpResponse(InvocationInfoProxy.getTenantid(),isvRequestBody);
    int resCode = httpResponse.getStatusLine().getStatusCode();
    HttpEntity entity = httpResponse.getEntity();
    String result = EntityUtils.toString(entity, "UTF-8");
    EntityUtils.consume(entity);
    resultEntity.setCode(resCode);
    resultEntity.setResult(result);
    return httpResponse;
  }

  @Override
  public CloseableHttpResponse doPost(CloseableHttpClient httpClient, String params,
      Map<String, Object> headerParams, HttpPost httpPost, ResultEntity resultEntity)
      throws IOException {
    if(!StringUtils.isEmpty(params)){
      httpPost.setEntity(new StringEntity(params,"UTF-8"));
    }
    URI uri = httpPost.getURI();
    String url = uri.toURL().toString();
    ISVRequestBody isvRequestBody = new ISVRequestBody();
    isvRequestBody.setUrl(url);
    isvRequestBody.setRequestMethod(POST);
    HashMap<String,String> headers = new HashMap<>();
    for (String key : headerParams.keySet()){
      headers.put(key,JSON.toJSONString(headerParams.get(key)));
    }
    Header[] allHeaders = httpPost.getAllHeaders();
    Arrays.stream(allHeaders).forEach(header -> {
      headers.put(header.getName(),header.getValue());
    });
    isvRequestBody.setHeaders(headers);
    isvRequestBody.setYhtAccessToken("yht_access_token_encryption");
    CloseableHttpResponse httpResponse = isvRequest.doCloseableHttpResponse(InvocationInfoProxy.getTenantid(),isvRequestBody);
    int resCode = httpResponse.getStatusLine().getStatusCode();
    HttpEntity entity = httpResponse.getEntity();
    String result = EntityUtils.toString(entity, "UTF-8");
    EntityUtils.consume(entity);
    resultEntity.setCode(resCode);
    resultEntity.setResult(result);
    return httpResponse;
  }

  @Override
  public CloseableHttpResponse doPostJson(CloseableHttpClient httpClient, String params,
      String contentType, Map<String, Object> headerParams, HttpPost httpPost,
      ResultEntity resultEntity) throws IOException {
    if (!StringUtils.isEmpty(params)) {
      StringEntity se = new StringEntity(params, "application/json", "utf-8");
      if (StringUtils.isEmpty(contentType)) {
        contentType = "application/json";
      }
      se.setContentType(contentType);
      httpPost.setEntity(se);
    }
    URI uri = httpPost.getURI();
    String url = uri.toURL().toString();
    ISVRequestBody isvRequestBody = new ISVRequestBody();
    isvRequestBody.setUrl(url);
    isvRequestBody.setRequestMethod(POST);
    HashMap<String,String> headers = new HashMap<>();
    for (String key : headerParams.keySet()){
      headers.put(key,JSON.toJSONString(headerParams.get(key)));
    }
    Header[] allHeaders = httpPost.getAllHeaders();
    Arrays.stream(allHeaders).forEach(header -> {
      headers.put(header.getName(),header.getValue());
    });
    isvRequestBody.setHeaders(headers);
    isvRequestBody.setYhtAccessToken("yht_access_token_encryption");
    CloseableHttpResponse httpResponse = isvRequest.doCloseableHttpResponse(InvocationInfoProxy.getTenantid(),isvRequestBody);
    int resCode = httpResponse.getStatusLine().getStatusCode();
    HttpEntity entity = httpResponse.getEntity();
    String result = EntityUtils.toString(entity, "UTF-8");
    EntityUtils.consume(entity);
    resultEntity.setCode(resCode);
    resultEntity.setResult(result);
    return httpResponse;
  }
}
