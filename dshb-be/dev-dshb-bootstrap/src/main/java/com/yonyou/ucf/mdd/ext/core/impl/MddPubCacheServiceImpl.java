//package com.yonyou.ucf.mdd.ext.core.impl;
//
//import com.yonyou.ucf.mdd.common.interfaces.context.IMddCacheService;
//import org.imeta.spring.support.cache.RedisManager;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.stereotype.Component;
//
//@Component("extendCache")
//@Deprecated
//public class MddPubCacheServiceImpl implements IMddCacheService {
//    private static final Logger log = LoggerFactory.getLogger(MddPubCacheServiceImpl.class);
//
//    @Qualifier("redisPubManager")
//    @Autowired(required = false)
//    private RedisManager redisPubManager;
//
//    @Override
//    public RedisManager getManager() {
//        return redisPubManager;
//    }
//
//    public void setRedisPubManager(RedisManager redisPubManager) {
//        this.redisPubManager = redisPubManager;
//    }
//}
