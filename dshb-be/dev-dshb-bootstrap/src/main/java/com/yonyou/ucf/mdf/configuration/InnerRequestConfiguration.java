package com.yonyou.ucf.mdf.configuration;

import com.yonyou.iuap.billcode.service.IBillNumberService;
import com.yonyou.ucf.mdd.ext.core.AppContext;
import com.yonyou.ucf.userdef.api.ISyncCharacterService;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class InnerRequestConfiguration {

  @Bean
  @ConditionalOnMissingBean
  public IBillNumberService IBillNumberService() {
    return (IBillNumberService) AppContext.getBean("billNumberService4YxyImpl");
  }

  @Bean
  public ISyncCharacterService ISyncCharacterService() {
    return (ISyncCharacterService) AppContext.getBean("syncCharacterService");
  }
}
