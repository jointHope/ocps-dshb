//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.yonyou.yonbip.iuap.xport.importing.progress.report.rest;

import cn.hutool.core.thread.BlockPolicy;
import com.yonyou.cloud.middleware.rpc.RPCStubBeanFactory;
import com.yonyou.iuap.yms.multitenant.UserExecutors;
import com.yonyou.ucf.mdd.ext.dubbo.DubboReferenceUtils;
import com.yonyou.ucf.mdd.thread.ThreadPoolBuilder;
import com.yonyou.yonbip.iuap.xport.common.support.network.AbstractXportRestTemplate;
import com.yonyou.yonbip.iuap.xport.importing.ImportSdkProperties;
import com.yonyou.yonbip.iuap.xport.importing.progress.report.SubTaskReportList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ImportProgressReportRemoteApiImpl implements ImportProgressReportApi {
    private static final Logger log = LoggerFactory.getLogger(ImportProgressReportRemoteApiImpl.class);
    public final ImportSdkProperties properties;
    public final AbstractXportRestTemplate template;
    protected final ExecutorService executor;

    public ImportProgressReportRemoteApiImpl(ImportSdkProperties properties, AbstractXportRestTemplate template) {
        this.properties = properties;
        this.template = template;
        this.executor = ThreadPoolBuilder.commonThreadPool().setThreadPoolName("import-progress-reporter-").setCorePoolSize(1).setMaximumPoolSize(Runtime.getRuntime().availableProcessors()).setKeepAliveTime(5L).setTimeUnit(TimeUnit.MINUTES).setWorkQueueSize(100).setRejectedExecutionHandler(new BlockPolicy()).build();
    }

    public void reportProgress(SubTaskReportList result) {
        UserExecutors.executeInDetachedThread(this.executor, () -> {
//            RPCStubBeanFactory rpChainBeanFactory = new RPCStubBeanFactory("iuap-metadata-import", "c87e2267-1001-4c70-bb2a-ab41f3b81aa3", "", ImportProgressReportApi.class);
//            rpChainBeanFactory.afterPropertiesSet();
//            ImportProgressReportApi reportApi = (ImportProgressReportApi)rpChainBeanFactory.getObject();

            ImportProgressReportApi reportApi = DubboReferenceUtils.getDubboService(ImportProgressReportApi.class,"iuap-metadata-import", null);
            try {
                reportApi.reportProgress(result);
            } catch (Exception var4) {
                log.error("exception when report import progress with rpc", var4);
            }

        });
    }

    public ImportProgressReportRemoteApiImpl(final ImportSdkProperties properties, final AbstractXportRestTemplate template, final ExecutorService executor) {
        this.properties = properties;
        this.template = template;
        this.executor = executor;
    }
}
