//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.yonyou.iuap.formula.engine.formulaset;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.yonyou.bip.user.service.IGlobalizationPreferenceService;
import com.yonyou.diwork.exception.BusinessException;
import com.yonyou.diwork.service.pub.ITenantUserService;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.iuap.formula.common.SpringContextHolder;
import com.yonyou.ucf.mdd.ext.dubbo.DubboReferenceUtils;
import com.yonyou.workbench.model.TenantVO;
import com.yonyou.workbench.model.UserVO;
import com.yonyoucloud.domain.service.IUserTenantService;
import java.util.Map;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DefaultSystemFormula {
    private static final Logger log = LoggerFactory.getLogger(DefaultSystemFormula.class);
    public static final String USER = "bpaas_formula_user_cache";
    public static final String TENANT_NAME = "bpaas_formula_tenant_name";

    public DefaultSystemFormula() {
    }

    public static String tenantId() {
        return InvocationInfoProxy.getTenantid();
    }

    public static String tenantName() throws BusinessException {
        String tenantName = (String)InvocationInfoProxy.getExtendAttribute("bpaas_formula_tenant_name");
        if (StringUtils.isEmpty(tenantName)) {
            ITenantUserService tenantUserService = (ITenantUserService)SpringContextHolder.getBean(ITenantUserService.class);
            TenantVO tenantVO = tenantUserService.getTenant(InvocationInfoProxy.getTenantid());
            tenantName = tenantVO == null ? "" : tenantVO.getTenantName();
            InvocationInfoProxy.setExtendAttribute("bpaas_formula_tenant_name", tenantName);
        }

        return tenantName;
    }

    public static String userId() {
        UserVO userVO = getUserVO();
        return userVO == null ? InvocationInfoProxy.getUserid() : userVO.getUserId();
    }

    public static String userName() {
        UserVO userVO = getUserVO();
        return userVO == null ? InvocationInfoProxy.getUsername() : userVO.getUserName();
    }

    public static String orgId() {
        UserVO userVO = getUserVO();
        return userVO != null && !StringUtils.isEmpty(userVO.getOrgId()) ? userVO.getOrgId() : "";
    }

    public static String orgName() {
        UserVO userVO = getUserVO();
        return userVO != null && !StringUtils.isEmpty(userVO.getOrgName()) ? userVO.getOrgName() : "";
    }

    private static UserVO getUserVO() {
        UserVO userVO = null;
        String userCache = (String)InvocationInfoProxy.getExtendAttribute("bpaas_formula_user_cache");

        try {
            if (!StringUtils.isBlank(userCache)) {
                userVO = (UserVO)JSON.parseObject(userCache, UserVO.class);
            }

            if (userVO == null) {
//                ITenantUserService tenantUserService = (ITenantUserService)SpringContextHolder.getBean(ITenantUserService.class);
//                IUserTenantService baseService = (IUserTenantService)SpringContextHolder.getBean(IUserTenantService.class);
                ITenantUserService tenantUserService = DubboReferenceUtils.getDubboService(ITenantUserService.class,"workbench-service", null);
                IUserTenantService baseService = DubboReferenceUtils.getDubboService(IUserTenantService.class,"u8c-baseservice", null);
                String userId = null;
                userId = InvocationInfoProxy.getUserid();
                Map orgMap;
                if (StringUtils.isEmpty(userId) || NumberUtils.isDigits(userId)) {
                    try {
                        orgMap = baseService.getUserTenant();
                        if (MapUtils.isNotEmpty(orgMap) && orgMap.containsKey("user")) {
                            userId = ObjectUtils.toString(((Map)orgMap.get("user")).get("userId"));
                        }
                    } catch (Exception var6) {
                    }
                }

                if (StringUtils.isNotEmpty(userId)) {
                    userVO = tenantUserService.getUserByUserId(userId);
                    orgMap = baseService.getOrgAndDeptByUserIds(Lists.newArrayList(new String[]{userId}));
                    if (MapUtils.isNotEmpty(orgMap) && orgMap.containsKey(userId)) {
                        userVO.setOrgId(((JSONObject)orgMap.get(userId)).getString("orgId"));
                        userVO.setOrgName(((JSONObject)orgMap.get(userId)).getString("orgName"));
                    }

                    InvocationInfoProxy.setExtendAttribute("bpaas_formula_user_cache", JSON.toJSONString(userVO));
                }
            }
        } catch (Exception var7) {
            log.error(var7.getMessage(), var7);
        }

        return userVO;
    }
}
