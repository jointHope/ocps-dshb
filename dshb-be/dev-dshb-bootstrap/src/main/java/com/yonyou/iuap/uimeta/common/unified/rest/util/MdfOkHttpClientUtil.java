//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by FernFlower decompiler)
//

package com.yonyou.iuap.uimeta.common.unified.rest.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.yonyou.iuap.context.InvocationInfoProxy;
import com.yonyou.iuap.uimeta.common.unified.rest.exception.MdfHttpClientException;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import com.yonyou.ucf.mdd.ext.core.AppContext;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequest;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.isvrequest.ISVRequestBody;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Request.Builder;
import org.apache.commons.collections4.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class MdfOkHttpClientUtil {
    private static final Logger logger = LoggerFactory.getLogger(MdfOkHttpClientUtil.class);
    private static OkHttpClient okHttpClient;
    private static ISVRequest isvRequest = AppContext.getBean(ISVRequest.class);

    public MdfOkHttpClientUtil() {
    }

    public static String postUrl(String url, Object param, Map<String, String> headerMap) throws MdfHttpClientException {
        try {
            if (InvocationInfoProxy.getYhtAccessToken()!=null && !InvocationInfoProxy.getYhtAccessToken().startsWith("btt")) {
                ISVRequestBody isvRequestBody = new ISVRequestBody();
                isvRequestBody.setUrl(url);
                isvRequestBody.setRequestMethod("POSTJSON");
                HashMap<String, String> hashMap = new HashMap<>();
                hashMap.put("data", JSONObject.toJSONString(param));
                isvRequestBody.setParams(hashMap);
                String yhtAccessToken = InvocationInfoProxy.getYhtAccessToken();
                if(headerMap == null) {
                    headerMap = new HashMap<>();
                }
                headerMap.put("yht_access_token", yhtAccessToken);
                isvRequestBody.setHeaders(headerMap);
                String result = isvRequest.doRequest(InvocationInfoProxy.getTenantid(), isvRequestBody);
                return result;
            }
            MediaType mediaType = MediaType.parse("application/json; charset=utf-8");
            RequestBody requestBody = FormBody.create(mediaType, JSON.toJSONString(param));
            Builder builder = (new Builder()).url(url).post(requestBody);
            addHeader(builder, headerMap);
            Response response = okHttpClient.newCall(builder.build()).execute();
            if (!response.isSuccessful()) {
                logger.error("访问外部系统异常 {}: {}", url, JSON.toJSONString(response));
                throw new RuntimeException("访问外部系统异常 " + url);
            } else {
                return response.body().string();
            }
        } catch (Exception var7) {
            throw new MdfHttpClientException("远程调用:" + url + "异常！异常信息:" + var7.getMessage(), var7);
        }
    }

    public static String getUrl(String url, Map<String, String> headerMap) throws MdfHttpClientException {
        try {
            Builder builder = (new Builder()).url(url).get();
            addHeader(builder, headerMap);
            Response response = okHttpClient.newCall(builder.build()).execute();
            if (!response.isSuccessful()) {
                logger.error("访问外部系统异常 {}: {}", url, JSON.toJSONString(response));
                throw new RuntimeException("访问外部系统异常 " + url);
            } else {
                return response.body().string();
            }
        } catch (Exception var4) {
            throw new MdfHttpClientException("远程调用:" + url + "异常！异常信息:" + var4.getMessage(), var4);
        }
    }

    private static void addHeader(Builder builder, Map<String, String> headerMap) {
        if (!MapUtils.isEmpty(headerMap)) {
            Iterator var2 = headerMap.entrySet().iterator();

            while(var2.hasNext()) {
                Entry<String, String> entry = (Entry)var2.next();
                String key = (String)entry.getKey();
                String value = (String)entry.getValue();
                if (key != null && value != null) {
                    builder.header(key, value);
                }
            }

        }
    }

    static {
        okHttpClient = (new okhttp3.OkHttpClient.Builder()).connectTimeout(10L, TimeUnit.SECONDS).writeTimeout(10L, TimeUnit.SECONDS).readTimeout(20L, TimeUnit.SECONDS).build();
    }
}
