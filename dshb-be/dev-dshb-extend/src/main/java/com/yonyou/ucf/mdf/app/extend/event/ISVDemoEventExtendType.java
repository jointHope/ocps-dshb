package com.yonyou.ucf.mdf.app.extend.event;

import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.module.isv.event.pojo.ISVEventType;

public class ISVDemoEventExtendType extends ISVEventType {
  public static final String ISV_TEST_TYPE = "ISV_TEST_TYPE";
  public static final String UIMETA_CACHE_CLEAN = "ISV_METACACHE_CLEAN";
  public static final String EVENT_BATCH_IMPORT = "EventBatchImport_isvSupportEventBatchImport";
}