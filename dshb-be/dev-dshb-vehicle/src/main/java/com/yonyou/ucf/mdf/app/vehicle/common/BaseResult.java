package com.yonyou.ucf.mdf.app.vehicle.common;

import com.yonyou.ucf.mdf.app.vehicle.exceptions.ExceptionCode;

import java.io.Serializable;

/**
 * ajax请求返回结果参数
 *
 * @author baizhihua
 */
public class BaseResult implements Serializable{

	private static final long serialVersionUID = 1L;
	private Boolean success;
    private String msg="";
    private String code;
    private String webSocketCode;
    private int forceUpdate;
    private Object data;
    
    
    public BaseResult(boolean success,String code,String message) {
        this.success = success;
        this.code = code;
        this.msg = message;
        this.forceUpdate=0; //0不强制更新 1 需要更新才能操作
    }

    //默认返回请求成功
    public BaseResult() {
        this.code = "0"; //默认成功
        this.success = true;
        this.forceUpdate=0; //0不强制更新 1 需要更新才能操作
    }

    public Boolean getSuccess() {
        if (success == null && code.equals("0"))
            success = true;
        return success;
    }

    public BaseResult(ExceptionCode exceptionCode) {
        this.msg = exceptionCode.getMsg();
        this.code = exceptionCode.getCode(); //默认请求成功
        this.success = exceptionCode.equals(ExceptionCode.Success);
        this.forceUpdate=0;
    }
    
    public String getWebSocketCode() {
		return webSocketCode;
	}

	public void setWebSocketCode(String webSocketCode) {
		this.webSocketCode = webSocketCode;
	}

	public int getForceUpdate() {
		return forceUpdate;
	}

	public void setForceUpdate(int forceUpdate) {
		this.forceUpdate = forceUpdate;
	}

	public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getCode() {
        if (code == null && success)
            code = "0";
        return code;
    }

    public void setCode(String code) {
        this.code = code;
        success = "0".equals(code);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
