package com.yonyou.ucf.mdf.app.vehicle.exceptions;

/**
 * @author lmc
 * @date 2018-04-03 10:49
 * @description
 */
public enum ExceptionCode {

    //通用的00
    AuthFailed("00001", "请登录后操作"),
    
    ServerError("-1", "服务异常"),
    ArgumentIllegal("-2", "参数错误"),
    Success("0", "请求成功");

    private String code;
    private String msg;

    ExceptionCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }

}
