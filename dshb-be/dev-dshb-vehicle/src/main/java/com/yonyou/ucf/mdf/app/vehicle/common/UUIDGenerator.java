package com.yonyou.ucf.mdf.app.vehicle.common;

import java.util.UUID;

public class UUIDGenerator {
	
	public static String nextId() {
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
	
}
