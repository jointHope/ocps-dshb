package com.yonyou.ucf.mdf.app.vehicle.model;

import java.io.Serializable;

public class UrbanepWechatConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    private String configId;

    private String accesstoken;

    private String jsticket;

    private String updateTime;

    public String getConfigId() {
        return configId;
    }

    public void setConfigId(String configId) {
        this.configId = configId == null ? null : configId.trim();
    }

    public String getAccesstoken() {
        return accesstoken;
    }

    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken == null ? null : accesstoken.trim();
    }

    public String getJsticket() {
        return jsticket;
    }

    public void setJsticket(String jsticket) {
        this.jsticket = jsticket == null ? null : jsticket.trim();
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime == null ? null : updateTime.trim();
    }
}