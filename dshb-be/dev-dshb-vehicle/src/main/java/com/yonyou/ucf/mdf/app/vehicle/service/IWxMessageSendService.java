package com.yonyou.ucf.mdf.app.vehicle.service;

import com.yonyou.ucf.mdf.app.vehicle.common.BaseResult;

/**
 * 微信公众号消息推送 业务层
 * 
 * @author smartpark
 */
public interface IWxMessageSendService {

    /**
     * 公众号消息推送
     * @param eventId
     * @param wechatOpenid
     * @param pushType
     * @param page
     * @return
     */
    BaseResult messagePush(String eventId, String wechatOpenid, String pushType, String page);

    /**
     * 微信小程序消息推送
     * @param eventId
     * @param wechatOpenid
     * @param pushType
     * @param page
     * @return
     */
    BaseResult appletMessagePush(String eventId, String wechatOpenid, String pushType, String page);

}
