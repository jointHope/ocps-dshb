package com.yonyou.ucf.mdf.app.vehicle.exceptions;

public class ServiceException extends BusinessException {

    private static final long serialVersionUID = 1L;

	public ServiceException(String code, String msg) {
        super(code, msg);
    }

}