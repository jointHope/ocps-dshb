package com.yonyou.ucf.mdf.app.vehicle.exceptions;

/**
 * @author lmc
 * @date 2018-04-03 10:49
 * @description
 */
public class BusinessException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    String code;
    String msg;

    public BusinessException(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}