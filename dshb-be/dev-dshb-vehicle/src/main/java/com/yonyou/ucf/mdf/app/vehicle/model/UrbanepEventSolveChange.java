package com.yonyou.ucf.mdf.app.vehicle.model;

import java.io.Serializable;
import java.util.Date;

public class UrbanepEventSolveChange implements Serializable {

    private static final long serialVersionUID = 1L;

    private String solveId;

    private String eventId;

    private String solveOper;

    private Date solveTime;

    private String solveResult;

    private String remark;

    private String operateType;

    public String getSolveId() {
        return solveId;
    }

    public void setSolveId(String solveId) {
        this.solveId = solveId == null ? null : solveId.trim();
    }

    public String getEventId() {
        return eventId;
    }

    public void setEventId(String eventId) {
        this.eventId = eventId == null ? null : eventId.trim();
    }

    public String getSolveOper() {
        return solveOper;
    }

    public void setSolveOper(String solveOper) {
        this.solveOper = solveOper == null ? null : solveOper.trim();
    }

    public Date getSolveTime() {
        return solveTime;
    }

    public void setSolveTime(Date solveTime) {
        this.solveTime = solveTime;
    }

    public String getSolveResult() {
        return solveResult;
    }

    public void setSolveResult(String solveResult) {
        this.solveResult = solveResult == null ? null : solveResult.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }

    public String getOperateType() {
        return operateType;
    }

    public void setOperateType(String operateType) {
        this.operateType = operateType == null ? null : operateType.trim();
    }
}