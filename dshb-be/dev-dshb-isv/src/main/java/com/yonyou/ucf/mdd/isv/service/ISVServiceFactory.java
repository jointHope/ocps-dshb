package com.yonyou.ucf.mdd.isv.service;

import com.yonyou.bip.user.service.IGlobalizationPreferenceService;
import com.yonyou.diwork.multilingual.service.ILanguageService;
import com.yonyou.diwork.ott.RobotInvocationGetter;
import com.yonyou.diwork.service.auth.IDiWorkPermissionService;
import com.yonyou.diwork.service.auth.IServiceIsolateService;
import com.yonyou.diwork.service.pub.ITenantUserService;
import com.yonyou.diwork.service.session.extend.ISessionManagerExtendService;
import com.yonyou.iuap.auth.api.orgperm.IOrgPermManagerService;
import com.yonyou.iuap.data.service.itf.FuncOrgDataQryService;
import com.yonyou.iuap.log.rpc.IBusinessLogService;
import com.yonyou.iuap.ruleengine.service.relevant.RelevantRuleMgmtService;
import com.yonyou.ucf.mdd.api.interfaces.rpc.*;
import com.yonyou.ucf.mdd.core.interfaces.rpc.RPCServiceAdapter;
import com.yonyou.ucf.mdf.iris.template.query.service.MdfTemplateQueryService;
import com.yonyou.ucf.transtype.service.itf.ITransTypeService;
import com.yonyou.yonbip.iuap.xport.importing.progress.report.rest.ImportProgressReportApi;
import com.yonyoucloud.bill.service.ITranslateService;
import com.yonyoucloud.domain.service.IUserTenantService;
import com.yonyoucloud.enm.service.IEnmQueryService;
import com.yonyoucloud.iuap.ucf.mdd.starter.ucg.openapi.isv.router.module.session.ISessionManagerInnerServiceApi;
import com.yonyoucloud.uretail.api.IBillQueryService;
import com.yonyoucloud.uretail.sys.pubItf.IDataPermissionService;
import com.yonyoucloud.uretail.sys.pubItf.IFuncPermissionService;
import com.yonyoucloud.uretail.sys.pubItf.IOrgPermissionService;


/**
 * <p>Title</p>
 * <p>Description</p>
 *
 * @Author chouhl
 * @Date 2020-05-13$ 11:07$
 * @Version 1.0
 **/
public interface ISVServiceFactory extends RPCServiceAdapter {

    IBillQueryService getBillQueryService(String group, String version, Integer timeout);

    IBillQueryService getBillQueryService(String group);

    <T> T getService(Class<T> clz, String group);

    IComOperateApi getIComOperateApiProxy(String group);

    IComQueryApi getIComQueryApiProxy(String group);

    IRefApi getIRefApiProxy(String group);

    IRuleApi getIRuleApiProxy(String group);

    IEnmQueryService getIEnumQueryService(String group);

    ISessionManagerInnerServiceApi getISessionManagerInnerService(String group);

    IFuncPermissionService getIFuncPermissionService(String group);

    IDataPermissionService getIDataPermissionService(String domain);

    IOrgPermissionService getIOrgPermissionService(String domain);

    ITranslateService getITranslateService(String group);

    IDiWorkPermissionService getIDiWorkPermissionService(String group);

    IBusinessLogService getIBusinessLogService(String group);

    ILanguageService getILanguageService(String group);

    FuncOrgDataQryService getFuncOrgDataQryService(String group);

    IOidAdapterApi getIOidAdapterApiProxy(String group);

    RelevantRuleMgmtService getRelevantRuleMgmtApiProxy(String group);

    //审批流回调缺少
    ISessionManagerExtendService getISessionManagerExtendService(String group);

    //审批流回调缺少
    ITransTypeService getITransTypeService(String group);

    MdfTemplateQueryService getMdfTemplateQueryService(String group);

    IOrgPermManagerService getOrgPermManagerService(String group);

    IServiceIsolateService getServiceIsolateService(String group);

    ImportProgressReportApi getImportProgressReportService(String group);

    IGlobalizationPreferenceService getIGlobalizationPreferenceService(String group);

    IPoiSupportApi getIPoiSupportService(String group);

    ITenantUserService getITenantUserService(String group);

    IUserTenantService getIUserTenantService(String group);

    RobotInvocationGetter getRobotInvocationGetter(String group);
}
